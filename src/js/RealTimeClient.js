import TokenStore from "./TokenStore";
import {EventEmitter} from "events";
import RestClient from "./RestClient";
import blobToArrayBuffer from "./blobToArrayBuffer";
import ops from "./ops";
import packer from "./msgpack";
import constants from "./constants";

class RealTimeClient extends EventEmitter {
  constructor(opts = {}) {
    super();

    this.handlers = {
      // hello
      0: this._handleHello,
      // heartbeat
      1: this._handleHeartbeat,
      // subscribe
      2: this._noop,
      // unsubscribe
      3: this._noop,
      // dispatch
      4: this._handleDispatch
    };

    if (!opts.noConnect) {
      console.log("RTC connecting initially");
      this._connect();
    }
    this._waiting = new Map();
    this._nonceCounter = 0;
  }

  async ensureConnection() {
    if (this.connected) {
      return;
    }
    if (!this._ws || !this._ws.connected) {
      if (!this._connecting) {
        console.log("ensureConnection triggering _connect()");
        await this._connect();
      }
      await new Promise(resolve => {
        this.once("ready", () => {
          resolve();
        });
      });
    }
  }

  async _subscribe(guildChannelMap) {
    await this.ensureConnection();
    const nonce = this._nonceCounter++;
    console.log("Sub!", guildChannelMap, nonce);
    if (typeof guildChannelMap.c == "string") {
      console.error(
        "WHAT ARE YOU DOING?!",
        new Error("GUILDCHANNELMAP.C IS A STRING")
      );
    }
    this._send(ops.subscribe, {_: guildChannelMap, n: nonce});
    return await new Promise(resolve => {
      this._waiting.set(nonce, details => {
        console.log("GOT ACK FOR SUB!", nonce, details);
        resolve(details);
        this._waiting.delete(nonce);
      });
    });
  }

  async _unsubscribe(guildChannelMap) {
    console.log("UNSUBBING!", guildChannelMap);
    await this.ensureConnection();
    const nonce = this._nonceCounter++;
    this._send(ops.unsubscribe, {_: guildChannelMap, n: nonce});
    return await new Promise(resolve => {
      this._waiting.set(nonce, () => {
        console.log("GOT ACK FOR UNSUB!", nonce, guildChannelMap);
        resolve();
        this._waiting.delete(nonce);
      });
    });
  }

  subscribe(guildChannelMap) {
    // throw new Error("Fix your code! Use stores!");
    // {
    //   guilds: {
    // 	123456: [1,2,3,4,5],
    // 	123457: true
    //   }
    // }
    const guildChannelObject = {};
    for (const [key, value] of guildChannelMap.entries
      ? guildChannelMap.entries()
      : Object.entries(guildChannelMap)) {
      guildChannelObject[key] = value === true || [...value.values()];
    }
    const nonce = this._nonceCounter++;
    this._send(ops.subscribe, {g: guildChannelObject, n: nonce});
    return new Promise(resolve => {
      this._waiting.set(nonce, () => {
        console.log("GOT ACK FOR SUB!", nonce);
        resolve();
        this._waiting.delete(nonce);
      });
    });
  }

  unsubscribe(guildChannelMap) {
    // throw new Error("Fix your code! Use stores!");
    const guildChannelObject = {};
    for (const [key, value] of guildChannelMap.entries
      ? guildChannelMap.entries()
      : Object.entries(guildChannelMap)) {
      guildChannelObject[key] = value === true || [...value.values()];
    }

    const nonce = this._nonceCounter++;

    this._send(ops.unsubscribe, {g: guildChannelObject, n: nonce});
    console.log("call to unimplemented fn unsub");

    return new Promise(resolve => {
      this._waiting.set(nonce, () => {
        console.log("GOT ACK FOR SUB!", nonce);
        resolve();
        this._waiting.delete(nonce);
      });
    });
  }

  _noop() {}

  async _handleDispatch(msg) {
    console.log("dispatch!~", msg);
    this.emit(msg.t, msg.d);
  }

  async _handleHello(msg) {
    this._heartbeatInterval = msg.h;
    this.emit("ready", msg);
  }

  async _handleHeartbeat() {
    this._send(ops.heartbeat);
    if (this._heartbeatTimeout) {
      clearTimeout(this._heartbeatTimeout);
    }
    this._heartbeatTimeout = setTimeout(() => {
      console.log("Hearbeat timed out... Reconnecting");
      if (this._ws) {
        this._ws.close();
      }
      console.log("_heartbeatTimeout triggering _connect()");
      this._connect();
    }, this._heartbeatInterval + 30000);
  }

  async setPresence(presence, activity) {
    await this._send(ops.presence, {
      presence,
      activity
    });
  }

  async joinMediaChannel(channelId) {
    const nonce = this._nonceCounter++;
    await this._send(ops.rtc, {c: channelId, n: nonce});
    return await new Promise(resolve => {
      this._waiting.set(nonce, data => {
        console.log("GOT ACK FOR RTC JOIN!", nonce, data);
        resolve({
          address: data.s
        });
        this._waiting.delete(nonce);
      });
    });
  }

  async _onMessage(event) {
    const msg = packer.decode(await blobToArrayBuffer(event.data));
    console.log("GET MSG!", msg, msg.d && msg.d.n);
    if (msg.d && msg.d.n !== null && msg.d.n !== undefined) {
      const waiting = this._waiting.get(msg.d.n);
      console.log(
        "NONCE?!",
        msg.d.n,
        waiting,
        this._waiting,
        msg.d && msg.d.n !== null && msg.d.n !== undefined
      );
      waiting(msg.d);
    } else {
      const handler = this.handlers[msg.o];
      if (!handler) {
        throw new Error(`Unimplemented OP: ${msg.o}`);
      }
      await handler.apply(this, [msg.d]);
    }
  }

  async _send(op, data) {
    try {
      this._ws.send(
        packer.encode({
          o: op,
          d: data
        })
      );
    } catch (err) {
      console.error(err);
      console.log("_send() failure calling _connect()");
      await this._connect();
      // await this.ensureConnection();
      // this._send(op, data);
    }
  }

  disconnect() {
    this.disconnecting = true;
    if (this._ws) this._ws.close();
  }

  async _connect() {
    console.log("_connect() called");
    if (this._connecting) {
      throw new Error("Already connecting!");
    } else if (this.disconnecting) this.disconnecting = false;
    this._connecting = true;
    if (this._heartbeatTimeout) {
      clearTimeout(this._heartbeatTimeout);
    }
    const token = await TokenStore.getToken();
    if (!token) {
      throw new Error("No token!");
    }
    if (!this.gatewayUrl) {
      const url = (await RestClient.get("/gateway")).url.replace(
        /wss?:/,
        new URL(constants.API_BASE).protocol == "https:" ? "wss:" : "ws:"
      );
      this.gatewayUrl = url;
    }
    if (this._ws) {
      this._ws.close();
    }
    this._ws = new WebSocket(this.gatewayUrl);
    this._ws.addEventListener("message", this._onMessage.bind(this));
    this._ws.addEventListener("close", () => {
      this.connected = false;
      if (this._disconnecting) {
        return console.log("Closed, NOT reconnecting; it was intended");
      }
      console.log("Closed, connecting in 1s");
      this._connecting = false;
      setTimeout(() => {
        console.log("_connect() timeout reached, connecting after it closed");
        this._connect();
      }, 1000);
    });
    await new Promise(resolve =>
      this._ws.addEventListener("open", () => {
        resolve();
      })
    );
    this._ws.send(token);
    // At some point ready data will be useful stuff
    await new Promise(resolve =>
      this.once("ready", readyData => {
        resolve(readyData);
      })
    );
    this._connecting = false;
    this.connected = true;
  }
}

const rtc = new RealTimeClient({noConnect: true});
window.rtc = rtc;

export default rtc;
