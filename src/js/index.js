import RealTimeClient from "./RealTimeClient";
import RestClient from "./RestClient";
import App from "../components/App.svelte";
import AppStore from "../stores/AppStore";
import GuildStore from "../stores/GuildStore";
import TokenStore from "./TokenStore";
import constants from "./constants";
import * as cryptipush from "./cryptipush";

if (navigator.serviceWorker) {
  navigator.serviceWorker
    .register("../sw.js", {scope: "/"})
    .then(async registration => {
      console.log("ServiceWorker registered!", registration);
    })
    .catch(err => {
      console.warn("Error installing service worker?", err);
    });
}

function onLoad(onLoaded) {
  if (
    document.readyState == "interactive" ||
    document.readyState == "complete"
  ) {
    onLoaded();
  } else {
    window.addEventListener("DOMContentLoaded", onLoaded);
  }
}

onLoad(() => {
  new App({
    target: document.getElementById("app-mount"),
    store: AppStore
  });
});
TokenStore.getToken().then(
  // No token, no push.
  token => (token ? initialise() : cryptipush.removeSubscription())
);

function initialise() {
  RestClient.get("/guilds").then(guilds => GuildStore.slurp(guilds));
  RestClient.get("/self/styles").then(res =>
    AppStore.set({userStyle: res.style})
  );
  RealTimeClient.ensureConnection().then(() => {
    console.log(
      "setPresence ensuredConnection",
      constants,
      constants.presenceReverse[localStorage.lastPresence],
      constants.presenceReverse[localStorage.lastPresence]
        ? Number(localStorage.lastPresence)
        : constants.presence.online
    );
    RealTimeClient.setPresence(
      constants.presenceReverse[localStorage.lastPresence]
        ? Number(localStorage.lastPresence)
        : constants.presence.online,
      localStorage.lastActivity || null
    );
  });
}

export default {
  initialise
};
