import env from "./constants";
import BN from "bn.js";

export default {
  // flake,
  generateSnowflake() {
    return Promise.resolve(
      new BN(Date.now() - env.epoch).shln(22).toString(10)
    );
  },
  when(snowflake) {
    return new Date(new BN(snowflake).shrn(22).toNumber() + env.epoch);
  }
};
