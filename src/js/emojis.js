import emojiCategories from "./emojiCategories.json";

export const colors = [
  "r1",
  "r2",
  "r3",
  "d1",
  "d2",
  "d3",
  "o1",
  "o2",
  "o3",
  "y1",
  "y2",
  "y3",
  "l1",
  "l2",
  "l3",
  "g1",
  "g2",
  "g3",
  "t1",
  "t2",
  "t3",
  "c1",
  "c2",
  "c3",
  "s1",
  "s2",
  "s3",
  "b1",
  "b2",
  "b3",
  "v1",
  "v2",
  "v3",
  "m1",
  "m2",
  "m3",
  "p1",
  "p2",
  "p3",
  "e1",
  "e2",
  "e3",
  "k1",
  "k2",
  "k3"
];

export const shapes = ["paw", "clw", "hmn"];

export const colorOverrideMap = {
  hmn: ["h1", "h2", "h3", "h4", "h5"],
  paw: ["fe1", "fk1", "ft1"]
};

export const colorOverrides = {};
const additionalColors = [];
for (const shape in colorOverrideMap) {
  for (const color of colorOverrideMap[shape]) {
    colorOverrides[color] = shape;
    additionalColors.push(color);
  }
}

export const allColors = colors.concat(additionalColors);

export const categories = Object.entries(emojiCategories)
  .sort(([nameA], [nameB]) => (nameA < nameB ? -1 : nameA > nameB ? 1 : 0))
  // .filter(([name]) => name != "utils")
  .map(([name, items]) => ({
    name,
    items: items.map(emoji => {
      const obj = {};
      const lastIndex = emoji.length - 1;
      const lastChar = emoji[emoji.length - 1];
      if (lastChar == "#") {
        obj.s = true;
        obj.c = true;
        obj.n = emoji.substring(0, lastIndex);
      } else if (lastChar == "!") {
        obj.c = true;
        obj.n = emoji.substring(0, lastIndex);
      } else if (lastChar == "@") {
        obj.s = true;
        obj.n = emoji.substring(0, lastIndex);
      } else {
        obj.n = emoji;
      }

      return obj;
    })
  }));

export const stems = categories.reduce(
  (collector, category) => collector.concat(category.items),
  []
);

export const emojiNames = stems
  .map(emoji => {
    if (emoji.c && emoji.s) {
      const output = [];
      for (const shape of shapes) {
        output.push(emoji.n + "_" + shape);
        for (const color of colors) {
          output.push(emoji.n + "_" + shape + "_" + color);
        }
        if (colorOverrideMap[shape]) {
          for (const color of colorOverrideMap[shape]) {
            output.push(emoji.n + "_" + shape + "_" + color);
          }
        }
      }
      return output;
    } else if (emoji.c) {
      const output = allColors.map(color => emoji.n + "_" + color);
      output.push(emoji.n);
      return output;
    } else if (emoji.s) {
      const output = shapes.map(shape => emoji.n + "_" + shape);
      return output;
    } else return [emoji.n];
  })
  .reduce((a, b) => a.concat(b), []);

export default emojiNames;
