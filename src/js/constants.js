if (typeof window !== "undefined") {
  window.self = window;
}

function urlB64ToUint8Array(base64String) {
  const padding = "=".repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding).replace(/-/g, "+").replace(/_/g, "/");

  const rawData = self.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

const exporting = {
  API_BASE:
    (typeof localStorage != "undefined" && localStorage.API_BASE) ||
    (location.hostname == "localhost"
      ? "http://localhost:3000/api"
      : location.origin + "/api"),
  PROXY_BASE:
    (typeof localStorage != "undefined" && localStorage.PROXY_BASE) ||
    (location.hostname == "localhost"
      ? "http://localhost:3022/proxy"
      : location.origin + "/proxy"),
  epoch: (process.env.EPOCH
    ? new Date(Date.UTC(process.env.EPOCH))
    : new Date(Date.UTC(2018, 1, 1))
  ).getTime(),
  presence: {
    offline: 0,
    online: 1,
    idle: 2,
    dnd: 3
  },
  permissions: {
    INVITE: 1 << 0,
    KICK: 1 << 1,
    BAN: 1 << 2,
    ADMIN: 1 << 3,
    MANAGE_CHANNELS: 1 << 4,

    VIEW_CHANNEL: 1 << 10,
    MESSAGE_CREATE: 1 << 11,
    MANAGE_MESSAGES: 1 << 13,

    MANAGE_NICKNAMES: 1 << 14,
    MANAGE_ROLES: 1 << 15,
    MANAGE_GUILD: 1 << 16,
    MENTION_EVERYONE: 1 << 17
  },
  friendlyPermissions: {},
  vapid: {
    publicKey: urlB64ToUint8Array(
      process.env.VAPID_PUBKEY ||
        "BC5kwKgoeEBL1ona8pBX6ZQjetS9fS3xObhE09SKJjXEXKqs5RYoUwi9T_dj5xpuIYwFV6BLF-VEVJ_Zp9Kb500"
    )
  },
  presenceReverse: {},
  permissionsReverse: {}
};

for (const permissionName in exporting.permissions) {
  exporting.permissionsReverse[
    exporting.permissions[permissionName]
  ] = permissionName;
}

console.log(exporting.presence);
for (const presenceName in exporting.presence) {
  console.log(presenceName, "it");
  exporting.presenceReverse[exporting.presence[presenceName]] = presenceName;
}

export default exporting;
