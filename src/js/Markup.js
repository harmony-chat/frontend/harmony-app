import MemberListStore from "../stores/MemberListStore";
import UserStore from "../stores/UserStore";
import {stems} from "../js/emojis.js";
import EmojiUtils from "./EmojiUtils";
import HashIcon from "../HashIcon.svg";
import MediaIcon from "../MediaIcon.svg";
import ChannelStore from "../stores/ChannelStore";
import constants from "./constants";
import AppStore from "../stores/AppStore";

export const nameRegex =
  "([`~^\\w\\d!#$%^&*()\\-_+=\\|\\]\\[\"':;,<>.?\\/][`~^\\w\\d!#$%^&*()\\-_+=\\|\\]\\[\"':;,<>.?\\/]{0,30}[`~^\\w\\d!#$%^&*()\\-_+=\\|\\]\\[\"':;,<>.?\\/])";

export function serialise(content, channelId) {
  const guildId = ChannelStore.getItem(channelId).guildId;
  for (const completion of completions.values()) {
    const matches = content.match(completion.regex);
    if (matches !== null) {
      for (const match of matches) {
        console.log(match);
        content = content.replace(
          match,
          completion.machineTransform(
            match.slice(1, -(completion.suffix || "").length || undefined),
            completion.getItems({guildId, channelId})
          )
        );
      }
    }
  }

  return content;
}

export const completions = new Map([
  [
    ":",
    {
      suffix: ":",
      nameKey: "n",
      idKey: "n",
      name: "Emoji",
      getItems() {
        return stems;
      },
      humanTransform(item) {
        const got = AppStore.get();
        return `:${EmojiUtils.diversify(
          item,
          got.emojiColor,
          got.emojiShape
        )}:`;
      },
      iconTransform(item) {
        const got = AppStore.get();
        return EmojiUtils.urlFor(
          EmojiUtils.diversify(item, got.emojiColor, got.emojiShape)
        );
      },
      machineTransform(name) {
        return `:${name}:`;
      }
    }
  ],
  [
    "@",
    {
      name: "Users",
      getItems({guildId, channelId}) {
        const hash = MemberListStore._calculateHash(guildId, channelId);
        console.log("HASH", hash, MemberListStore.getItem(hash));
        return MemberListStore.getItem(hash)
          .map(group => group[1])
          .reduce((group, collector) => collector.concat(group), [])
          .map(userId => {
            const user = UserStore.getItem(userId);
            user.name = user.username;
            return user;
          });
      },
      humanTransform(item) {
        return "@" + item.username;
      },
      iconTransform(user) {
        return user && user.avatar
          ? constants.API_BASE + "/avatars/" + user.id + "/" + user.avatar
          : constants.DEFAULT_AVATAR;
      },
      machineTransform(username, users) {
        const user = users.find(user => user.username == username);
        console.log(user, username, users);
        if (user) {
          return `<@${user.id}>`;
        } else {
          return `@${username}`;
        }
      }
    }
  ],
  [
    "#",
    {
      name: "Channels",
      getItems({guildId}) {
        return ChannelStore.getItems(guildId);
      },
      humanTransform(item) {
        return "#" + item.name;
      },
      iconTransform(item) {
        if (item && item.media) {
          return MediaIcon;
        } else {
          return HashIcon;
        }
      },
      machineTransform(name, items) {
        const channel = items.find(item => item.name == name);
        if (channel) {
          return `<#${channel.id}>`;
        } else {
          return `#${name}`;
        }
      }
    }
  ]
]);

for (const [completionKey, completion] of completions.entries()) {
  completion.regex = new RegExp(
    `${completionKey}${nameRegex}${completion.suffix || ""}`,
    "gi"
  );
}
