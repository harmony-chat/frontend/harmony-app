import * as mediasoupClient from "mediasoup-client";
import TokenStore from "./TokenStore";
import {EventEmitter} from "events";
import RealTimeClient from "./RealTimeClient";
import mediaConstants from "./mediaConstants";
import msgpack from "./msgpack";
import blobToArrayBuffer from "./blobToArrayBuffer";

// We'll need to change this later
const LocalVolumeStore = {
  getVolume() {
    return 1;
  }
};

export const sockets = new Map();

export const clients = new Map();

export class MediaSocketClient extends EventEmitter {
  constructor(address) {
    super();
    this._address = address;
    this._waiting = new Map();

    this._handlers = {
      [mediaConstants.ops.hello](data) {
        this._heartbeatInterval = data.h;
        console.log("We're ready", data.h);
        this.emit("ready");
      },
      [mediaConstants.ops.mediasoup](data) {
        console.log("Got mediasoup req", data);
        this.emit("mediasoup-" + data.c, data);
      },
      [mediaConstants.ops.heartbeat]() {
        console.log("uwu", this, "Heartbeating!");
        this._send(mediaConstants.ops.heartbeat);
        if (this._heartbeatTimeout) {
          clearTimeout(this._heartbeatTimeout);
        }
        this._heartbeatTimeout = setTimeout(() => {
          console.log("Hearbeat timed out... Reconnecting");
          if (this._socket) {
            this._socket.close();
          }
          console.log("_heartbeatTimeout triggering _connect()");
          this._connect();
        }, this._heartbeatInterval + 30000);
      }
    };
  }

  async _onMessage(event) {
    console.log(event);
    try {
      var msg = msgpack.decode(await blobToArrayBuffer(event.data));
    } catch (err) {
      throw err;
    }

    console.log("Got msg", msg, msg.d, msg.d && msg.d.n);

    if (msg.d && msg.d.n !== undefined && msg.d.c) {
      const nonceHandler = this._waiting.get(msg.d.c + "-" + msg.d.n);
      console.log(
        "Well? firing nonce handler",
        nonceHandler,
        msg.d.c + "-" + msg.d.n
      );
      nonceHandler(msg.d.d);
    }

    const handler = this._handlers[msg.o];
    if (handler) {
      try {
        await handler.apply(this, [msg.d]);
      } catch (err) {
        console.error(
          `Error while running handler for op: ${msg.o}!`,
          msg,
          event,
          err
        );
        throw err;
      }
    } else {
      console.error("Handler for op doesn't exist!", msg.o, msg);
      throw new Error("Handler for op doesn't exist!");
    }
  }

  async sendSoup(data) {
    return await this._send(mediaConstants.ops.mediasoup, data);
  }

  async _send(op, data) {
    console.log("SENDING", op, data);
    const frame = msgpack.encode({
      o: op,
      d: data
    });

    this._socket.send(frame);
  }

  async _connect() {
    const token = await TokenStore.getToken();
    this._socket = new WebSocket(this._address);
    this._socket.addEventListener("message", this._onMessage.bind(this));
    this._socket.addEventListener("close", () => {
      console.log("Connecting in 1s");
      this._connecting = false;
      setTimeout(() => {
        console.log("_connect() timeout reached, connecting after it closed");
        this._connect();
      }, 1000);
    });

    await new Promise(resolve =>
      this._socket.addEventListener("open", () => {
        resolve();
      })
    );
    this._socket.send(token);
    // At some point ready data will be useful stuff
    await new Promise(resolve =>
      this.once("ready", readyData => {
        resolve(readyData);
      })
    );
    this._connecting = false;
    this.connected = true;
  }

  listen(roomId, nonce) {
    return new Promise((resolve, reject) => {
      const killer = setTimeout(() => {
        this._waiting.delete(roomId + "-" + nonce);
        reject(new Error("Timed out"));
      }, 8000);
      this._waiting.set(roomId + "-" + nonce, response => {
        clearTimeout(killer);
        this._waiting.delete(roomId + "-" + nonce);
        resolve(response);
      });
      console.log("Waiting now on", roomId + "-" + nonce);
    });
  }

  async ensureConnection() {
    if (!this._socket || !this._socket.connected) {
      if (!this._connecting) {
        console.log("MSC ensureConnection triggering _connect()");
        await this._connect();
      } else {
        await new Promise(resolve => {
          this.once("ready", () => {
            resolve();
          });
        });
      }
    }
  }
}

export class MediaClient extends EventEmitter {
  constructor(channelId) {
    super();
    this.channelId = channelId;

    this._subscriptions = new Map();
    this.streams = new Map();
    this._tracks = new Map();
    this._producers = new Map();

    this._screenShares = new Map();

    this._nonceCounter = 0;
    this._room = null;
    this._socketAddress = null;
    this._connect();
    this._listening = false;

    console.log("Setting...", clients);
    clients.set(channelId, this);
  }

  _listen() {
    if (this._listening) {
      return;
    }
    this._onPacketsBound = this._onPackets.bind(this);
    this.socket.on(`mediasoup-${this.channelId}`, this._onPacketsBound);
    this._listening = true;
  }

  _shutdown() {
    if (this._onPacketsBound)
      this.socket.removeListener(
        `mediasoup-${this.channelId}`,
        this._onPacketsBound
      );

    clients.delete(this.channelId);
  }

  _onPackets(data) {
    console.log("Mediasoup packet!", data.d, this, this._room);
    if (data.d && data.d.notification) {
      this._room.receiveNotification(data.d);
    }
  }

  async _connect() {
    const mediaData = await RealTimeClient.joinMediaChannel(this.channelId);
    console.log(mediaData);
    this._socketAddress = mediaData.address;
    this._listen();
    this.emit("state", "Socket connecting");
    await this.socket.ensureConnection();
    console.log("ensureConnection to socket done");
    this.emit("state", "Socket connected");
    this._room = new mediasoupClient.Room({requestTimeout: 8000});
    this._setupRoom();
    const userId = TokenStore.getUserIdSync(this.socket.token);
    console.log("Joining room now");
    await this._room.join(userId);
    this._receiver = this._room.createTransport("recv");
    this._onJoined();
    this.emit("joined");
    this.emit("state", "Joined");
    console.log("Joined room");

    // Now add our stored devices!
    const devices =
      localStorage.currentDevices && JSON.parse(localStorage.currentDevices);
    if (devices) {
      (async () => {
        for (const deviceType in devices) {
          for (const deviceId in devices[deviceType]) {
            if (devices[deviceType][deviceId]) {
              try {
                const stream = await navigator.mediaDevices.getUserMedia({
                  [deviceType]: {
                    deviceId: deviceId
                  }
                });

                const tracks = stream.getTracks();
                for (const track of tracks) {
                  await this.addTrack(track, deviceId);
                }
              } catch (err) {
                console.warn("Couldn't find device!", deviceId, err);
              }
            }
          }
        }
      })();
    }
  }

  async addTrack(track, mediaId) {
    track.track = track;
    this._tracks.set(track.id, track);
    const producer = this._createProducer(track, mediaId);
    if (!this._sender) {
      console.log("Creating sender transport");
      this._sender = this._room.createTransport("send");
    }
    const res = await producer.send(this._sender);
    console.log(res, "Producer response?");
    console.log(producer, this._sender);

    this._emitPeer(this._room.peerName);

    return producer;
  }

  _createProducer(track, mediaId) {
    const producer = this._room.createProducer(track);
    const producers = this._producers.get(mediaId);
    producer.on("close", () => {
      console.log("Producer close");
      const producers = this._producers.get(mediaId);
      console.log("Producers", producers);
      if (producers) producers.delete(track.id);
      console.log("Producers", producers && producers.size);
      if (producers && !producers.size) {
        console.log("Deleting producer");
        this._producers.delete(mediaId);
      }
      this._tracks.delete(track.id);
      track.stop();
      if (this._room) this._emitPeer(this._room.peerName);
    });
    producer.on("trackended", () => producer.close());
    if (producers) producers.set(track.id, producer);
    else this._producers.set(mediaId, new Map([[track.id, producer]]));
    return producer;
  }

  getParticipants() {
    return this._room.peers.filter(peer => !peer.closed).map(peer => ({
      id: peer.name,
      consumers: peer.consumers
    }));
  }

  getTracks(peerId) {
    if (peerId == this._room.peerName) {
      return Array.from(this._tracks.values());
    } else {
      const peer = this._room.getPeerByName(peerId);
      return peer.consumers;
    }
  }

  nonce() {
    return this._nonceCounter++;
  }

  _setupRoom() {
    console.log("Setting up room", this._room);
    this._room.on("newpeer", peer => {
      this._setupPeer(peer);
      this._emitPeerList();
    });
    console.log("Listening for notify");
    this._room.on("notify", notification => {
      this.socket.sendSoup({c: this.channelId, d: notification});
    });
    this._room.on("close", () => {
      console.log("Room closed");
      this._room = null;
      this.emit("disconnect");
      this._shutdown();
    });
    this._room.on("request", (request, callback, errback) => {
      const nonce = this.nonce();
      this.socket.sendSoup({c: this.channelId, d: request, n: nonce});
      this.socket
        .listen(this.channelId, nonce)
        .then(callback)
        .catch(errback);
    });
  }

  _onJoined() {
    this._room.peers.forEach(peer => this._setupPeer(peer));
    this._emitPeerList();
  }

  _emitPeerList() {
    console.log(this, this._subscriptions);
    for (const {peerId, hook} of this._subscriptions.values()) {
      if (peerId === null) {
        hook();
      }
    }
  }

  _setupPeer(peer) {
    console.log("NEW PEER", peer);
    peer.on("close", () => {
      this._emitPeerList();
    });
    peer.on("newconsumer", async consumer => {
      console.log("PEER PLAYERS CONSUMER", consumer);
      await this._onConsumer(peer, consumer);

      this._emitPeer(peer.name);
    });
    peer.consumers.forEach(consumer => this._onConsumer(peer, consumer));

    this._emitPeer(peer.name);
  }

  _emitPeer(changedPeerId) {
    for (const {peerId, hook} of this._subscriptions.values()) {
      if (peerId == changedPeerId) {
        hook();
      }
    }
  }

  async _onConsumer(peer, consumer) {
    console.log("GOT A CONSUMER!", this, new Error("lol"));
    consumer.on("close", () => {
      this._emitPeer(peer.name);
    });

    if (consumer.kind == "audio") {
      console.log("ABOUT TO RECEIVE", this._receiver, this);
      await (consumer.__trackPromise = consumer
        .receive(this._receiver)
        .then(track => {
          const stream = new MediaStream();
          stream.addTrack(track);
          const audio = new Audio();
          audio.srcObject = stream;
          audio.autoplay = true;
          audio.volume = LocalVolumeStore.getVolume(peer.name);
          const playRequest = audio.play();
          if (playRequest && playRequest.then) {
            playRequest
              .then(() => console.log("Playing consumer"))
              .catch(err => console.warn("Not playing consumer because", err));
          }
          this.streams.set(consumer.id, {stream, audio, track, consumer});
        }));
    }
  }

  async playConsumer(consumer) {
    if (consumer instanceof MediaStreamTrack) {
      const streamData = this.streams.get(consumer.id);
      const stream = (streamData && streamData.stream) || new MediaStream();

      if (!streamData) {
        stream.addTrack(consumer);
        this.streams.set({stream});
      }

      return {
        pause: () => {},
        stream
      };
    } else {
      const existingStream = this.streams.get(consumer.id);

      if (consumer.track && consumer.locallyPaused) {
        await consumer.resume();
      }

      if (existingStream) {
        return {
          pause: () => consumer.pause(),
          stream: existingStream.stream
        };
      }

      if (consumer._transport && !consumer.track) {
        await consumer.__trackPromise;

        const existingStream = this.streams.get(consumer.id);
        return {
          pause: () => consumer.pause(),
          stream: existingStream.stream
        };
      }

      // Gross! But what more can we do??
      const track = consumer._transport
        ? consumer.track || (await consumer.__trackPromise)
        : await (consumer.__trackPromise = consumer.receive(this._receiver));

      const stream = new MediaStream();
      stream.addTrack(track);

      this.streams.set(consumer.id, {stream, track, consumer});

      return {stream, pause: () => consumer.pause()};
    }
  }

  subscribe(peerId, component, hook) {
    const had = this._subscriptions.has(hook);
    this._subscriptions.set(hook, {
      peerId,
      hook: hook.bind(component)
    });

    if (!had) {
      component.on("destroy", () => this._subscriptions.delete(hook));
    }

    hook.apply(component);
  }

  disconnect() {
    this._room.leave();
    this.emit("disconnect");
  }

  get socket() {
    if (!this._socketAddress) {
      return undefined;
    }
    const oldSocket = sockets.get(this._socketAddress);
    if (oldSocket) {
      return oldSocket;
    }
    const socket = new MediaSocketClient(this._socketAddress);
    sockets.set(this._socketAddress, socket);
    return socket;
  }
}

window.mclient = MediaClient;
window.mclient.clients = clients;
window.mclient.sockets = sockets;
