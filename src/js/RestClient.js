import TokenStore from "./TokenStore";
import constants from "./constants";

async function request(method, path, noParse, body) {
  const token = await TokenStore.getToken();
  console.log(body, "body", path);
  const response = await fetch(constants.API_BASE + path, {
    method,
    headers: {
      Authorization: token,
      "Content-Type": body
        ? body instanceof Blob ? "application/octet-stream" : "application/json"
        : undefined
    },
    body: body instanceof Blob ? body : body && JSON.stringify(body),
    redirect: "follow"
  });
  if (response.status < 200 || response.status > 299) {
    throw {
      message: response.statusText,
      code: response.status,
      body: await response.text()
    };
  }
  if (noParse) {
    return await response.arrayBuffer();
  } else {
    return await response.json();
  }
}

export const RestClient = {
  get: (path, noParse) => request("GET", path, noParse),
  head: (path, noParse) => request("HEAD", path, noParse),
  post: (path, body, noParse) => request("POST", path, noParse, body),
  put: (path, body, noParse) => request("PUT", path, noParse, body),
  delete: (path, body, noParse) => request("DELETE", path, noParse, body),
  patch: (path, body, noParse) => request("PATCH", path, noParse, body),

  request
};

export default RestClient;
