export default function fastInsert(arr, key) {
  // TODO: For batch insertions, we could call Array.prototype.concat() once we found where the edge messages belong which would be much quicker than recalculating each time!
  // we use lastIndexOf because chances are it's recent
  const existingIndex = arr.lastIndexOf(key);
  if (existingIndex == -1) {
    let targetIndex = 0;
    // If the first item in the array was bigger, we can insert at the back...
    if (arr.length && !(arr[0].length > key.length || arr[0] > key)) {
      // If there's only one item, we can short to here as we already know it's bigger
      if (key.length == 1) {
        targetIndex = 1;
      } else {
        for (let i = arr.length - 1; i != -1; i--) {
          // If the index is newer, we insert right after it
          // Since the next one would be older
          if (arr[i].length < key.length || arr[i] < key) {
            targetIndex = i + 1;
            break;
          }
        }
      }
    }
    arr.splice(targetIndex, 0, key);
  }
}
