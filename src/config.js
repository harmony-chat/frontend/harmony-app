import icon from "./icon.png";
const configObject = {
  siteName: process.env.SITE_NAME || "Harmony",
  canonicalUrl: process.env.CANONICAL_URL || "http://localhost:1234/",
  icon
};
configObject.description =
  process.env.SITE_DESCRIPTION ||
  `${
    configObject.siteName
  } is a new kind of chat service that's lightweight, optimised for slow networks, and blazing fast!`;

export default configObject;
