import constants from "./js/constants";
import url from "url";
import cryptipush from "./js/cryptipush";

const CACHE_NAME = "harmony";

self.addEventListener("install", event => {
  self.skipWaiting();

  // TODO: Figure out how we can cache only a tiny 'connecting'-type page?
  console.log("INSTALLING!");
  event.waitUntil(
    caches.open(CACHE_NAME).then(cache => {
      console.log("CACHE OPENED");
      return cache
        .addAll(["/", "/index.html"])
        .catch(err => {
          console.error(err);
        })
        .then(res => {
          console.log("CACHE ADDED", res);
          return res;
        });
    })
  );
});

self.addEventListener("fetch", event => {
  console.log("FETCHING!", event);
  event.respondWith(
    fetch(event.request)
      .then(response => {
        // Check if we received a valid response
        if (
          !response ||
          response.status !== 200 ||
          response.type !== "basic" ||
          // Don't cache it because it tells us where to find new versions of js
          url.parse(event.request.url).pathname == "/" ||
          event.request.url.includes(constants.API_BASE)
        ) {
          return response;
        }

        // IMPORTANT: Clone the response. A response is a stream
        // and because we want the browser to consume the response
        // as well as the cache consuming the response, we need
        // to clone it so we have two streams.
        const responseToCache = response.clone();

        caches.open(CACHE_NAME).then(cache => {
          cache.put(event.request, responseToCache);
        });

        return response;
      })
      .catch(err => {
        console.warn(err);
        return caches.match(event.request).then(response => {
          console.log("Cache hit?", response);
          // Cache hit - return response
          if (response) {
            return response;
          } else {
            throw err;
          }
        });
      })
  );
});

self.addEventListener("notificationclick", event => {
  const notification = event.notification;
  const {channelId, messageId, guildId} = notification.data;
  // const action = event.action;

  // if(action == "reply") {
  //   // not implemented
  //   notification.close();
  // } else {
  self.clients.openWindow(
    `${self.location.protocol}//${
      self.location.host
    }/#/channels/${guildId}/${channelId}/${messageId}`
  );
  notification.close();
  // }
});

self.addEventListener("pushsubscriptionchange", event => {
  event.waitUntil(
    cryptipush
      .subscribe()
      .then(subscription => cryptipush.publishSubscription(subscription))
  );
});

self.addEventListener("push", event => {
  console.log("GOT A PUSH!", event);
  const {data} = event.data.json();

  console.log("PUSH DATA", data);

  if (data.type == "MESSAGE") {
    event.waitUntil(
      self.registration.showNotification(
        `@${data.data.authorName} (#${data.data.channelName}) ${
          data.data.guildName
        }`,
        {
          tag: data.data.channelId,
          body: data.data.content,
          icon:
            constants.API_BASE +
            (data.data.authorAvatarURL || constants.DEFAULT_AVATAR),
          vibrate: [100, 50, 100],
          data: {
            url: location.origin + data.data.url
          },
          actions: [{action: "reply", title: "Reply"}]
        }
      )
    );
  }
});
