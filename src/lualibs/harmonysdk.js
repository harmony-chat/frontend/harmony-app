import lua from "@mstrodl/fengari/src/lua";
import lualib from "@mstrodl/fengari/src/lualib";
import lauxlib from "@mstrodl/fengari/src/lauxlib";
import fengari from "@mstrodl/fengari/src/fengaricore";

// This is all running inside the webworker!

// we generate ids locally because we can't await because fuck lua
let pathCounter = 0;

function send(L, action, data) {
  postMessage({
    source: "gameworker",
    destination: "renderer",
    data,
    action,
    type: "command",
    stateId: L.__stateId
  });
}

addEventListener("message", function(ev) {
  if (
    ev.data &&
    ev.data.source == "renderer" &&
    ev.data.destination == "gameworker"
  ) {
    if (ev.data.type == "action") {
      if (ev.data.actionName == "create") {
        const L = lauxlib.luaL_newstate();
        self.zoop = L;
        L.__stateId = ev.data.stateId;
        const state = genState(L);

        lua.lua_newtable(L);
        state.listenerTableIndex = lauxlib.luaL_ref(L, lua.LUA_REGISTRYINDEX);

        lualib.luaL_openlibs(L);

        lauxlib.luaL_requiref(
          L,
          fengari.to_luastring("harmonysdk"),
          function(L) {
            lauxlib.luaL_newlib(L, harmonySDK);
            return 1;
          },
          1
        );
        lua.lua_pop(L, 1);

        lauxlib.luaL_loadstring(L, fengari.to_luastring(ev.data.code));
        console.log(Array.from(L.stack));
        lua.lua_call(L, 0, 0);
      }
    } else if (ev.data.type == "event") {
      if (ev.data.stateId === null) {
        for (const L of stateIds.values()) {
          const state = genState(L);

          registerListener(ev, L, state);
        }
      } else {
        const L = stateIds.get(ev.data.stateId);
        const state = states.get(L);

        registerListener(ev, L, state);
      }
    }
  }
});

function registerListener(ev, L, state) {
  const listeners = state.listeners.get(ev.data.eventName);
  for (const listenerRef of listeners.values()) {
    lua.lua_rawgeti(L, lua.LUA_REGISTRYINDEX, state.listenerTableIndex);

    lua.lua_rawgeti(L, -1, listenerRef);

    let argCount = 0;
    for (const arg of ev.data.args) {
      if (typeof arg == "undefined") {
      } else {
        if (typeof arg == "number") {
          lua.lua_pushnumber(L, arg);
        } else if (typeof arg == "string") {
          lua.lua_pushliteral(L, arg);
        } else {
          console.warn(arg);
          throw new Error("Argument is of invalid type!");
        }
        argCount++;
      }
    }

    lua.lua_call(L, argCount, 0);
    // lua.lua_pop(L, 2);
  }
}

const states = new Map();
const stateIds = new Map();

function genState(L) {
  const existing = states.get(L);
  if (existing) return existing;

  const state = {
    listeners: new Map(
      ["mousedown", "mouseup", "mousemove", "unload", "load", "repaint"].map(
        key => [key, new Set()]
      )
    ),
    listenerIndex: null
  };
  states.set(L, state);
  stateIds.set(L.__stateId, L);

  return state;
}

const harmonySDK = {
  addListener(L) {
    const eventName = fengari.to_jsstring(lauxlib.luaL_checkstring(L, 1));
    const state = genState(L);
    lauxlib.luaL_checktype(L, 2, lua.LUA_TFUNCTION);
    lua.lua_rawgeti(L, lua.LUA_REGISTRYINDEX, state.listenerTableIndex);
    lua.lua_pushvalue(L, 2);
    const listener = lauxlib.luaL_ref(L, -2);
    console.log(eventName);
    console.log(state.listeners);
    state.listeners.get(eventName).add(listener);
    return 0;
  }
};

function generateCall(name, args = []) {
  harmonySDK[name] = function(L) {
    console.log("yar", lua.lua_tonumberx(L, 1));

    const finalArgs = [];
    for (const argIndex in args) {
      console.log(argIndex);
      const arg = args[argIndex];
      console.log(arg);
      console.log("yar", lua.lua_tonumberx(L, 1));
      console.log("aaa", `lauxlib["luaL_" + ${arg}](L, ${argIndex} + 1))`);
      const value = lauxlib["luaL_" + arg](L, Number(argIndex) + 1);
      finalArgs.push(
        arg.endsWith("string") ? fengari.to_jsstring(value) : value
      );
    }

    send(L, name, finalArgs);

    return 0;
  };
}

function repeat(arg, count) {
  const arr = new Array(count);
  for (let i = 0; i != count; i++) {
    arr[i] = arg;
  }

  return arr;
}

generateCall("clearRect", repeat("checknumber", 4));
generateCall("fillRect", repeat("checknumber", 4));
generateCall("strokeRect", repeat("checknumber", 4));

generateCall(
  "fillText",
  ["checkstring"].concat(repeat("checknumber", 2).concat(["optnumber"]))
);
generateCall(
  "strokeText",
  ["checkstring"].concat(repeat("checknumber", 2).concat(["optnumber"]))
);

// Paths

generateCall("beginPath");
generateCall("closePath");
generateCall("moveTo", repeat("checknumber", 2));
generateCall("lineTo", repeat("checknumber", 2));
generateCall("bezierCurveTo", repeat("checknumber", 6));
generateCall("quadraticCurveTo", repeat("checknumber", 4));
generateCall("arc", repeat("checknumber", 5).concat(["optboolean"]));
generateCall("arcTo", repeat("checknumber", 5));
generateCall("ellipse", repeat("checknumber", 7).concat(["optboolean"]));
generateCall("rect", repeat("checknumber", 4));

// Drawing paths

// Path2D is an int for us
generateCall("fill", ["optint", "optstring"]);
generateCall("stroke", ["optint"]);
// drawFocus
generateCall("scrollPathIntoView", ["optint"]);
generateCall("clip", ["optint", "optstring"]);
generateCall("isPointInPath", ["optint"].concat(repeat("checknumber", 2)));
generateCall("isPointInStroke", ["optint"].concat(repeat("checknumber", 2)));

// Transformations

generateCall("rotate", ["checknumber"]);
generateCall("scale", repeat("checknumber", 2));
generateCall("translate", repeat("checknumber", 2));
generateCall("transform", repeat("checknumber", 6));
generateCall("setTransform", repeat("checknumber", 6));
generateCall("resetTransform");

// Drawing images

generateCall(
  "drawImage",
  ["checkstring"].concat(
    repeat("optnumber", 4),
    repeat("checknumber", 2),
    repeat("optnumber", 2)
  )
);

// Pixel manipulation

// The canvas state

generateCall("save");
generateCall("restore");

// Hit regions

// Path2D
harmonySDK.createPath = function(L) {
  const pathId = pathCounter++;

  send(L, "createPath", pathId);

  lua.lua_pushinteger(L, pathId);
  return 1;
};

generateCall("pathAddPath", ["checkint", "checkint"]);
generateCall("pathClosePath", ["checkint"]);
generateCall("pathMoveTo", ["checkint", "checknumber", "checknumber"]);
generateCall("pathLineTo", ["checkint", "checknumber", "checknumber"]);
generateCall(
  "pathBezierCurveTo",
  ["checkint"].concat(repeat("checknumber", 6))
);
generateCall(
  "pathQuadraticCurveTo",
  ["checkint"].concat(repeat("checknumber", 4))
);
generateCall(
  "pathArc",
  ["checkint"].concat(repeat("checknumber", 5), "optbool")
);
generateCall("pathArcTo", ["checkint"].concat(repeat("checknumber", 5)));
generateCall(
  "pathEllipse",
  ["checkint"].concat(repeat("checknumber", 7), "optbool")
);
generateCall("pathRect", ["checkint"].concat(repeat("checknumber", 4)));
// respond with another game
generateCall("respond", ["checkstring"]);
