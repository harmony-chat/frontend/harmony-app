import RestClient from "../js/RestClient";
import RealTimeStore from "./RealTimeStore";

class StyleStore extends RealTimeStore {
  constructor() {
    super();
    super._setup();
  }

  storeName = "GUILD_STYLE";
  subscriptionKey = "g";

  async _fetchData(guildId) {
    return await RestClient.get(`/guilds/${guildId}/styles`);
  }
}

export default new StyleStore();
