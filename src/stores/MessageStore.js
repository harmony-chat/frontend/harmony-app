import {EventEmitter} from "events";
import UserStore from "./UserStore";
import RealTimeClient from "../js/RealTimeClient";

function fastInsert(arr, key) {
  // TODO: For batch insertions, we could call Array.prototype.concat() once we found where the edge messages belong which would be much quicker than recalculating each time!
  // we use lastIndexOf because chances are it's recent
  const existingIndex = arr.lastIndexOf(key);
  if (existingIndex == -1) {
    let targetIndex = 0;
    // If the first item in the array was bigger, we can insert at the back...
    if (arr.length && !(arr[0].length > key.length || arr[0] > key)) {
      // If there's only one item, we can short to here as we already know it's bigger
      if (key.length == 1) {
        targetIndex = 1;
      } else {
        for (let i = arr.length - 1; i != -1; i--) {
          // If the index is newer, we insert right after it
          // Since the next one would be older
          if (arr[i].length < key.length || arr[i] < key) {
            targetIndex = i + 1;
            break;
          }
        }
      }
    }
    arr.splice(targetIndex, 0, key);
  }
}

function fastDelete(arr, key) {
  const existingIndex = arr.lastIndexOf(key);
  if (existingIndex !== -1) arr.splice(existingIndex, 1);
}

class MessageStore extends EventEmitter {
  constructor() {
    super();
    this.storage = new Map();
    this.metaStore = new Map();
  }

  slurp(messages) {
    console.log("fuck were slurping MESSAGES", messages, new Error("hcecjer"));
    let single = !!(messages.id || messages.nonce);
    let inserted = false;
    if (single) {
      inserted = true;
      this._slurpOne(messages);
    } else {
      for (const message of messages.values ? messages.values() : messages) {
        inserted = true;
        this._slurpOne(message);
      }
    }
    console.log(messages, "We got these messages...");
    if (inserted)
      this.emit(
        "changed",
        single
          ? new Set([messages])
          : messages.values
            ? messages
            : new Set(messages)
      );
  }

  unslurp(messages) {
    console.log("fuck were unslurping MESSAGES", new Error("mlem!"));
    let single = !!messages.id;
    if (single) {
      this._unslurpOne(messages);
    } else {
      for (const message of messages.values ? messages.values() : messages) {
        this._unslurpOne(message);
      }
    }

    this.emit(
      "changed",
      single
        ? new Set([messages])
        : messages instanceof Set
          ? messages
          : new Set(messages)
    );
  }

  _unslurpOne(message) {
    const existed = this.storage.get(message.id);
    // No point continuing if it doesn't exist in one storage map
    if (existed) {
      if (existed.nonce) this.storage.delete(existed.nonce);
      if (existed.id) this.storage.delete(existed.id);

      const metaGot = this.metaStore.get(existed.channelId);
      if (metaGot && metaGot.length) {
        if (existed.id) fastDelete(metaGot, existed.id);
        if (existed.nonce) fastDelete(metaGot, existed.nonce);
      }
      this.emit(`updated_${existed.id}`, null);
    }
  }

  _slurpOne(message) {
    if (message.author) UserStore._slurpOne(message.author);
    console.log("one slurperoo");
    const chosenKey = message.nonce || message.id;
    const exists = this.storage.get(chosenKey);
    const oldMessage = exists || {};
    for (const key in message) {
      oldMessage[key] = message[key];
    }
    this.storage.set(chosenKey, oldMessage);
    if (message.nonce && message.id) {
      this.storage.set(message.id, oldMessage);
    } else if (oldMessage.nonce && oldMessage.id) {
      this.storage.set(oldMessage.nonce, oldMessage);
    }
    const finalKey = oldMessage.nonce || oldMessage.id;
    if (message.channelId) {
      const oldMeta = this.metaStore.get(message.channelId);
      if (oldMeta) {
        // TODO: It would probably be faster to insert all entries at once...
        fastInsert(oldMeta, finalKey);
      } else this.metaStore.set(message.channelId, [finalKey]);
    }
    if (exists) {
      this.emit(`updated_${finalKey}`, oldMessage);
    } else {
      this.emit("new", oldMessage);
    }
  }

  getMessages(channelId, asArray) {
    const result = {};
    if (channelId) {
      const messageIds = this.metaStore.get(channelId);

      if (asArray) {
        if (!messageIds || !messageIds.length) return [];
        return messageIds.map(messageId => this.storage.get(messageId));
      }

      if (!messageIds || !messageIds.length) return {};
      for (const messageId of messageIds) {
        result[messageId] = this.storage.get(messageId);
      }
      return result;
    } else {
      // Should we sort this?
      if (asArray) return Array.from(this.storage.values());
      for (const message of this.storage.values()) {
        result[message.id] = message;
      }
      return result;
    }
  }

  getMessage(messageId) {
    return this.storage.get(messageId);
  }
}

RealTimeClient.on("MESSAGE_UPDATE", function(msg) {
  console.log("UPDATE! yum yum");
  store.slurp(msg);
});

RealTimeClient.on("MESSAGE_CREATE", function(msg) {
  console.log("slurp yum yum");
  store.slurp(msg);
});

RealTimeClient.on("MESSAGE_DELETE", function(msg) {
  console.log("DELETE yum yum");
  store.unslurp(msg);
});

RealTimeClient.on("ATTACHMENT_CREATE", function(attachment) {
  const msg = store.getMessage(attachment.messageId);
  // Not our problem I guess
  if (!msg) return console.log("No message..?", msg, attachment);
  const found = msg.attachments
    .map((a, i) => ({a, i}))
    .find(({a}) => a.id == attachment.id);
  console.log("Attachment created...");
  if (found) {
    const attachmentIndex = found.i;
    msg.attachments[attachmentIndex] = attachment;
  } else {
    msg.attachments.push(attachment);
  }
  console.log("So!", msg.attachments);
  store.slurp(msg);
});

const store = new MessageStore();
window.msgs = store;
export default store;
