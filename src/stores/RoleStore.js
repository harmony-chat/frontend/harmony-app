import RestClient from "../js/RestClient";
import RealTimeStore from "./RealTimeStore";

class RoleStore extends RealTimeStore {
  constructor() {
    super();
    this._positions = new Map();
    super._setup();
  }

  storeName = "ROLE";
  subscriptionKey = "g";

  _setPosition(roleId, position) {
    const existing = this.getItem(roleId);
    if (!existing) {
      this._storage.set(roleId, {id: roleId, position});
    } else {
      existing.position = position;
      this._storage.set(roleId, existing);
    }
  }

  _merge(existingRole, roleUpdate) {
    let changed = false;

    if (
      roleUpdate.position !== undefined &&
      existingRole.position !== undefined &&
      existingRole.position !== roleUpdate.position
    ) {
      const existingPositions = this._positions.get(existingRole.guildId);
      const id = this._getId(existingRole);
      if (existingPositions) {
        const oldPosition = existingRole.position;
        // Meh.. This could definitely be faster
        const newPositions = new Map(existingPositions.entries());

        if (roleUpdate.position < oldPosition) {
          console.log("new < old");
          for (let i = roleUpdate.position; i < oldPosition; i++) {
            const positionedRole = existingPositions.get(i);
            if (!positionedRole) {
              console.log("Position doesn't exist?!", i);
              continue;
            }
            this._setPosition(positionedRole, i + 1);
            newPositions.set(i + 1, positionedRole);
          }
        } else {
          console.log("new > old");
          for (let i = oldPosition + 1; i <= roleUpdate.position; i++) {
            const positionedRole = existingPositions.get(i);
            if (!positionedRole) {
              console.log("Position doesn't exist?!", i);
              continue;
            }
            this._setPosition(positionedRole, i - 1);
            newPositions.set(i - 1, positionedRole);
          }
        }
        newPositions.set(roleUpdate.position, id);
        this._positions.set(existingRole.guildId, newPositions);
      } else {
        this._positions.set(
          existingRole.guildId,
          new Map([[roleUpdate.position, id]])
        );
      }
      this._setPosition(id, roleUpdate.position);
    }

    for (const newProperty in roleUpdate) {
      existingRole[newProperty] = roleUpdate[newProperty];
      changed = true;
    }

    return changed;
  }

  // unslurp(role) {
  //   if (!role.id) return console.warn("Unslurping without a roleId?");
  //   const existing = this.storage.get(role.id);
  //   if (!existing) return;
  //   let position = existing.position;
  //   // When we hit a gap, we should be safe to drop
  //   const positions = this.positions.get(role.guildId);
  //   if (positions) {
  //     let next = positions.get(++position);
  //     while (next !== undefined) {
  //       const nextRole = this.storage.get(next);
  //       if (nextRole.position == 0)
  //         console.warn("Would force this role into zero?!");
  //       else nextRole.position--;
  //       next = positions.get(++position);
  //     }
  //   }
  //   const metadata = this.metadata.get(role.guildId);
  //   if (metadata) metadata.delete(role.id);
  //   this.storage.delete(role.id);
  //   this.emit("change", role);
  // }

  getRole(id) {
    return this.getItem(id);
  }

  getRoles(guildId) {
    const existing = this._metadata.get(guildId);
    return existing
      ? Array.from(existing.values()).map(roleId => this.getItem(roleId))
      : [];
  }

  async _fetchData(guildId) {
    return await RestClient.get(`/guilds/${guildId}/roles`);
  }

  _storeNew(id, storeType) {
    this._store(id, storeType);
    const metaId = this._getMetadataId(storeType);
    let metadata = this._metadata.get(metaId);
    if (metadata) metadata.add(id);
    else metadata = new Set([metaId]);
    // Safer to just set() it always otherwise we could have issues
    this._metadata.set(metaId, metadata);

    if (storeType.position !== undefined) {
      console.log(
        "Setting a position for a guild's role...",
        storeType.position
      );
      const existingPositions = this._positions.get(metaId);
      if (existingPositions) {
        const overwritten = existingPositions.get(storeType.position);
        if (overwritten) {
          if (overwritten != id) {
            console.warn(
              "Overwriting a position! This is bad!",
              overwritten,
              id,
              new Error("traceback")
            );
          }
        } else {
          existingPositions.set(storeType.position, id);
          this._positions.set(metaId, existingPositions);
        }
      } else {
        this._positions.set(metaId, new Map([[storeType.position, id]]));
      }
    }
  }
}

const store = new RoleStore();

window.rolestore = store;

export default store;
