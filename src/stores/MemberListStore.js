import GenericStore from "./GenericStore";
import RestClient from "../js/RestClient";
import OverrideStore from "./OverrideStore";
import RoleStore from "./RoleStore";
import murmurhash from "murmurhash";
import constants from "../js/constants";

export class MemberListStore extends GenericStore {
  constructor() {
    super();
    super._setup();
  }

  storeName = "MEMBER_LIST";

  // noop, we have no ws events here
  _attachListeners() {}

  async fetchChunk(guildId, channelId, {after = "", before = ""}) {
    const memberList = await RestClient.get(
      `/channels/${channelId}/members?after=${after}&before=${before}`
    );

    memberList.id = this._calculateHash(guildId, channelId);

    this.slurp(memberList);
  }

  _merge(oldItem, newItem) {
    let changed = false;

    for (const section of newItem) {
      const oldSection = oldItem.find(
        oldSection => section[0] == oldSection[0]
      );
      if (oldSection) {
        for (const userId of section[1]) {
          if (!oldSection.includes(userId)) {
            oldSection.push(userId);
            changed = true;
          }
        }
      } else {
        oldItem.push(section);
        changed = true;
      }
    }

    return changed;
  }

  _calculateHash(guildId, channelId) {
    let totalPerms = "";
    let zero = true;
    // const globalRole = RoleStore.getRole(guildId);
    // const globalView =
    //   globalRole &&
    //   globalRole.permissions &
    //     (constants.permissions.VIEW_CHANNEL ===
    //       constants.permissions.VIEW_CHANNEL);
    const overrides = OverrideStore.getItems(channelId);
    for (const override of overrides) {
      if (
        override.deny &
        (constants.permissions.VIEW_CHANNEL ===
          constants.permissions.VIEW_CHANNEL)
      ) {
        totalPerms += "-" + override.roleId;
        zero = false;
      } else if (
        override.allow &
        (constants.permissions.VIEW_CHANNEL ===
          constants.permissions.VIEW_CHANNEL)
      ) {
        totalPerms += "+" + override.roleId;
        const role = RoleStore.getRole(override.roleId);
        zero = false;
      }
    }
    const hash = zero ? 0 : murmurhash.v3(totalPerms);
    return guildId + "-" + hash;
  }
}

export default new MemberListStore();
