import RestClient from "../js/RestClient";
import RealTimeStore from "./RealTimeStore";

class UserStore extends RealTimeStore {
  constructor() {
    super();
    this._fetching = new Map();
    super._setup();
  }

  storeName = "USER";
  subscriptionKey = "u";

  _subscriptionItemId(subscriptionKey, storeType) {
    if (subscriptionKey == "u") {
      return storeType.id || storeType.userId;
    } else {
      throw new Error("Unknown subscriptionKey: " + subscriptionKey);
    }
    // return this._metadataId();
  }

  getUsers(rawMap) {
    if (rawMap) return this._storage;
    const result = {};
    for (const user of this._storage.values()) {
      result[user.id] = user;
    }
    return result;
  }

  // We're actually slightly different, we don't do metadata,
  // we just use normal ids
  _metadataId() {
    return undefined;
  }
  async _fetchData(userId) {
    return await RestClient.get(`/users/${userId}`);
  }

  // We need to figure this out honestly
  fetchUser(userId) {
    console.log("Slow! Fetching user: ", userId, new Error("eek"));
    return (async () => {
      const fetching = this._fetching.get(userId);
      if (fetching) {
        await fetching;
        return this.getItem(userId);
      } else {
        const existing = this.getItem(userId);
        if (existing) return existing;
        const fetchingPromise = this.fetch(userId);
        this._fetching.set(userId, fetchingPromise);
        await fetchingPromise;
        this._fetching.delete(userId);
        return this.getItem(userId);
      }
    })();
  }

  getUser(userId) {
    return this.getItem(userId);
  }
}

const store = new UserStore();

export default store;
