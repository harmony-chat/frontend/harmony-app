import RestClient from "../js/RestClient";
import RealTimeStore from "./RealTimeStore";

class OverrideStore extends RealTimeStore {
  constructor() {
    super();
    super._setup();
  }

  subscriptionKey = "c";
  storeName = "CHANNEL_OVERRIDES";

  async _fetchData(channelId) {
    return await RestClient.get(`/channels/${channelId}/overrides`);
  }
}

export default new OverrideStore();
