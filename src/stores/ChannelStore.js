import RealTimeStore from "./RealTimeStore";
import RestClient from "../js/RestClient";

class ChannelStore extends RealTimeStore {
  constructor() {
    super();
    super._setup();
  }

  storeName = "CHANNEL";
  subscriptionKey = "g";

  getChannel(channelId) {
    return this.getItem(channelId);
  }

  async fetchChannel(channelId) {
    const channel = await RestClient.get(`/channels/${channelId}`);
    this.slurp(channel);
    return channel;
  }

  async _fetchData(itemId, key) {
    if (key == "c") {
      return await RestClient.get(`/channels/${itemId}`);
    } else {
      console.log("Fetching...", itemId);
      return await RestClient.get(`/guilds/${itemId}/channels`);
    }
  }
}

export default new ChannelStore();
