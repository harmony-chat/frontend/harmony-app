module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: ["eslint:recommended", "prettier"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
    process: "readonly"
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module"
  },
  parser: "babel-eslint",
  settings: {
    "html/html-extensions": [".html", ".svelte"]
  },
  plugins: ["html", "prettier"],
  rules: {
    indent: ["warn", 2],
    "linebreak-style": ["error", "unix"],
    quotes: ["error", "double"],
    semi: ["error", "always"],
    "no-console": "off",

    "prettier/prettier": [
      "error",
      {
        semi: true,
        bracketSpacing: false
      }
    ]
  }
};
