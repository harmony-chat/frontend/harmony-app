const fs = require("fs");
const path = require("path");
const utils = require("util");

const readdirPromise = utils.promisify(fs.readdir);
const writeFilePromise = utils.promisify(fs.writeFile);

// Shamelessly stolen! https://stackoverflow.com/a/5827895/5067741
function walk(dir, done) {
  let results = [];
  fs.readdir(dir, function(err, list) {
    if (err) return done(err);
    let pending = list.length;
    if (!pending) return done(null, results);
    list.forEach(function(fileName) {
      const file = path.resolve(dir, fileName);
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function(err, res) {
            results = results.concat(res);
            if (!--pending) done(null, results);
          });
        } else {
          results.push(file);
          if (!--pending) done(null, results);
        }
      });
    });
  });
}

function walkPromise(dir) {
  return new Promise((resolve, reject) => {
    walk(dir, (err, res) => {
      if (err) reject(err);
      else resolve(res);
    });
  });
}

const blacklist = new Set /*["utils"]*/();

const emojiList = require(process.env.MUSTD_JSON_PATH);

const humanColors = ["h1", "h2", "h3", "h4", "h5"];

const colors = [
  "r1",
  "r2",
  "r3",
  "d1",
  "d2",
  "d3",
  "o1",
  "o2",
  "o3",
  "y1",
  "y2",
  "y3",
  "l1",
  "l2",
  "l3",
  "g1",
  "g2",
  "g3",
  "t1",
  "t2",
  "t3",
  "c1",
  "c2",
  "c3",
  "s1",
  "s2",
  "s3",
  "b1",
  "b2",
  "b3",
  "v1",
  "v2",
  "v3",
  "m1",
  "m2",
  "m3",
  "p1",
  "p2",
  "p3",
  "e1",
  "e2",
  "e3",
  "k1",
  "k2",
  "k3",
  "fe1",
  "ft1",
  "fk1"
];

const modifiers = [
  "_paw",
  "_hmn",
  "_clw",
  ...colors.map(color => "_paw_" + color),
  ...colors.map(color => "_hmn_" + color),
  ...humanColors.map(color => "_hmn_" + color),
  ...colors.map(color => "_clw_" + color),
  ...colors.map(color => "_" + color),
  ...humanColors.map(color => "_" + color)
];

(async () => {
  const emojiMap = {};

  const categories = await readdirPromise(process.env.MUSTD_PATH);
  await Promise.all(
    categories.filter(category => !blacklist.has(category)).map(category =>
      walkPromise(path.join(process.env.MUSTD_PATH, category)).then(
        files =>
          (emojiMap[category.replace(/_/g, " ")] = files
            .map(file => path.parse(file).name)

            .map(file => {
              if (
                modifiers.some(modifier => file.endsWith(modifier)) &&
                !file.endsWith("_hand_modifier") &&
                !file.startsWith("color_modifier_")
              ) {
                const modifier = modifiers.find(modifier =>
                  file.endsWith(modifier)
                );
                console.log("Found modifier", modifier, file);
                return file.slice(0, -modifier.length);
              } else return file;
            })
            .filter((x, i, self) => self.indexOf(x) === i)

            .sort((a, b) => (a < b ? -1 : a > b ? 1 : 0))

            .map(emoji => {
              const color = emojiList.roots[emoji];
              const shape =
                color && emojiList.roots[emoji].includes("clw_default");

              if (color && shape) emoji += "#";
              else if (shape) emoji += "@";
              else if (color) emoji += "!";

              return emoji;
            }))
      )
    )
  );

  await writeFilePromise(
    "./emojiCategories.generated.json",
    JSON.stringify(emojiMap),
    "utf8"
  );
})();
