module.exports = {
  plugins: [
    "@babel/plugin-proposal-class-properties",
    [
      "@babel/plugin-transform-runtime",
      {
        regenerator: true
      }
    ]
  ]
};

// if (process.env.NODE_ENV == "production") {
//   module.exports.plugins.push([
//     "transform-remove-console",
//     {exclude: ["error", "warn"]}
//   ]);
// }
