FROM node:11-stretch as builder

ENV NODE_ENV production

WORKDIR /app

RUN apt-get update && \
    apt-get install build-essential -y

RUN npm i -g node-gyp

COPY package*.json yarn.lock ./
RUN yarn --production=false
COPY . .


ENV SITE_NAME Harmonica
ENV CANONICAL_URL https://harmonica.gq/
ENV VAPID_PUBKEY BC5kwKgoeEBL1ona8pBX6ZQjetS9fS3xObhE09SKJjXEXKqs5RYoUwi9T_dj5xpuIYwFV6BLF-VEVJ_Zp9Kb500

RUN yarn build:production

FROM registry.access.redhat.com/ubi8/nginx-120 AS nginx

# COPY nginx-site.conf /etc/nginx/conf.d/default.conf

COPY --from=builder /app/dist/* /opt/app-root/src

CMD nginx -g "daemon off;"
